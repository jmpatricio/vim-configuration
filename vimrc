"-------------------------------"
"--------- Basic stuff ---------"

syntax enable 			                                    " enable syntac
set tabstop=4 shiftwidth=4 softtabstop=4 expandtab smarttab	" Use propper indentation
let mapleader = ','		                                    " change leader
set number			                                        " enable line numbers
set backspace=indent,eol,start  " make the backspace work like other editors
set nocompatible              	" we want the latest settings
filetype off                  	" required for vundle

"-------------------------------"
"--------- Visual --------------"
colorscheme atom-dark				" enable color scheme
set guifont=Source_Code_Pro_Light:h13 	" set the font
"set guifont=/Users/joao/Library/Fonts/Sauce\ Code\ Powerline\ Regular:h13 	" set the font
let g:Powerline_symbols = 'fancy'

" Remove the scrollbars
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R

set guioptions-=e				" replace tabs with vim tabs

" Set the linennumber background equals to the main bg
hi  LineNr guibg=bg				 

" Set fake left padding for the window
set foldcolumn=1
hi foldcolumn guibg=bg

" Replace the vertical split colors
hi vertsplit  guifg=bg guibg=white
hi horizSplit guifg=bg guibg=white

" Display the always status bar
set laststatus=2

"----------------------------"
"--------- Searching --------"
set hlsearch					" set highlight for search
set incsearch					" incremental search


"----------------------------"
"--------- Splitting--------"

set splitbelow " first below
set splitright " then right

" simplify splitting with only control + vim navigation keys
nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

"----------------------------"
"--------- Mappings ---------"

" tap ,ev and edit the .vimrc in a new tab
nmap <Leader>ed :tabedit ~/.vimrc<cr> 
nmap <Leader>edp :tabedit ~/.vim/plugins.vim<cr> 

" Add simple highlight removal 
nmap <Leader><space> :nohlsearch<cr>

" Add cmd+1 to display the file tree
nmap <D-1> :NERDTreeToggle<cr>

" Add control+O to search tags
nmap <C-O> :CtrlPBufTag<cr>

" Add control+e to recent files
nmap <C-E> :CtrlPMRUFiles<cr>

" Tag search
nmap <Leader>f :tag

" Laravel php artisan dump
nmap <Leader>ld :!php artisan dump

" Laravel 4 Specific

nmap <Leader><Leader>c :CtrlP app/controllers/api/v1/<cr>
nmap <Leader><Leader>u :CtrlP app/ucademics/<cr>
nmap <Leader><Leader>s :CtrlP app/ucademics/services/<cr>
nmap <Leader><Leader>t :CtrlP app/ucademics/transformers/<cr>

"----------------------------"
"--------- Auto-Commands ----"

" Load configuration after save
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source %
	autocmd BufWritePost .plugins.vim source %
augroup END


"----------------------------"
"--------- Vundle -----------"
so ~/.vim/plugins.vim


"----------------------------"
"--------- Plugin Custom Settings  -----------"

"/*
"/ CtrlP
"/*

" Ignore tmp files, swap, and vcs files
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'

" Open ctrlp in a new tab by default
let g:ctrlp_prompt_mappings = {
    \ 'AcceptSelection("e")': ['<c-t>'],
    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
    \ }
"/
"/ NERDTRee
"/
"
let NERDTreeHijackNetrw = 0

"/
"/ GReplace 
"/
set grepprg=ag
let g:grep_cmd_opts = "--line-numbers --noheading"


"/
"/ PHP Autocompletion
"/

" Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
autocmd FileType php setlocal omnifunc=phpcomplete_extended#CompletePHP

let g:phpcomplete_index_composer_command = "/usr/local/bin/composer"


"/
"/ Syntastic
"/
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_php_checkers=['php', 'phpcs']
let g:syntastic_php_phpcs_args='--standard=PSR2 -n'


"/
"/ Airliner
"/
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts=1

" Notes and tips
" - Press zz to center the line
" - 
